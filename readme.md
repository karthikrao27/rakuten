
# Requirements,
 - JDK 1.8
 - Maven
 - Chrome(chrome latest version)
 - Mac or Windows or Linux (the binry file is 64 bit )

# How To Run:
  To run a default Suite, 

  > mvn clean test -Dsuite=assignmentSuite
  
  if you want to run with a different product, you can change 'pdpUrl' in data.properties.
  Make sure you are seleting a product with no extra attribute

 Once the execution is done, you view the results in 
 
 > /target/surefire-reports/index.html

Few Interesting things to note:
1. how duplicate elements can be avoided, 
you can have a look at how Address form is filled in Checkout page - eg method like - selectGender

2. Advantages of Fluent Wait
Look at Page Objects -> isLoaded method


# Project Structure:
src/main/java/assign/rakuten/marketplace/pages            > Package contains Page Objects  
src/main/java/assign/rakuten/marketplace/commonutils      > Package contains Utils helpful for PageObjects   
src/main/java/assign/rakuten/marketplace/dto              > To contains Common info  
src/test/java/assign/rakuten/marketplace/data             > Contains data constants   
src/test/java/assign/rakuten/marketplace/annotations      > Contains Project specific annotations   
src/test/java/assign/rakuten/marketplace/helper           > Contains Helpers for the Page Objects   
src/test/java/assign/rakuten/marketplace/*Tests.java      > Contains Tests   
src/test/resources/data/data.properties                   > Contains testdata  
src/test/resources/drivers                                > Contains binary drivers   
src/test/resources/suiteXml                               > Contains testNg suites   


# Test Cases Info -> LoginAcceptanceTest:
1. verifySuccessfulLogin (To verify a successful login happens on providing correct credentials)
2. verifyLoginForWrongPassword (To verify a error message on providing wrong password)
3. verifyLoginForWrongEmail (To verify a error message on providing wrong email)
4. verifyLoginForWrongEmailFormat (To verify a error message on providing wrong email format)
5. verifyLoginForEmptyEmailEmptyPassword (To verify error message on submitting empty email and password)

# Test Cases Info -> WorkflowTests:
1. verifyCheckoutAsExistingLoginUser (To Verify Existing logged-in User is able to checkout to the Payment page with Default Billing Address)
2. verifyCheckoutAsExistingLoginUserWithNewAddress (To Verify Existing logged-in User is able to checkout to the Payment page with New Address)
3. verifyCheckoutAsGuest (To Verify Guest User is able to checkout to the Payment page)
