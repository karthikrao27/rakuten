package assign.rakuten.marketplace.commonutils;

import assign.rakuten.marketplace.pages.IPage;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class PageObjectUtils {

    public static String[] MONTHS = new String[] {"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"};

    private WebDriver driver;

    public PageObjectUtils(WebDriver driver) {
        this.driver = driver;
    }

    public void loadPage(String url) {
        this.driver.navigate().to(url);
    }

    public void scrollTo(WebElement elem) {
        this.executeJS("window.scrollTo(arguments[0], arguments[1]);", elem.getLocation().getX(), elem.getLocation().getY());
    }

    public void scrollIntoView(WebElement elem) {
        this.executeJS("arguments[0].scrollIntoView(true);", elem);
    }

    public void sleep(int seconds){
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
        }
    }

    public <T> void sleepTill(T t, Function<T,Boolean> function, int seconds){
        Long now = new Date().getTime();
        Long timeout = now + (seconds*1000);
        while(timeout>new Date().getTime()) {
            if (function.apply(t)) break;
        }
    }

    public WebElement fluentWait(final By locator)
    {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(this.driver).withTimeout(50, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement webElement = wait.until(new Function<WebDriver, WebElement>()
        {
            public WebElement apply(WebDriver driver)
            {
                return driver.findElement(locator);

            }});
        return webElement;
    };

    public <WebElement, T> T waitUntil(WebElement webElement, Function<WebElement , T> function, int timeOutInSeconds){
        T condition = null;
        try{
            Wait<WebElement> wait = new FluentWait<WebElement>(webElement).withTimeout(timeOutInSeconds, TimeUnit.SECONDS)
                    .pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class);
            condition = wait.until(function);
        }catch(TimeoutException e){
            if (condition instanceof Boolean) condition = (T) new Boolean(false);
        }
        return condition;
    }

    public <T> boolean waitUntil(Function<? super WebDriver, T> function, int timeOutInSeconds){
        boolean condition = true;
        try{
            Wait<WebDriver> wait = new FluentWait<WebDriver>(this.driver).withTimeout(timeOutInSeconds, TimeUnit.SECONDS)
                    .pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class);
            wait.until(function);
        }catch(TimeoutException e){condition=false;}

        return condition;
    }

    public <T> boolean waitUntil(IPage page, int timeOutInSeconds){
        boolean condition = true;
        try{
            Wait<WebDriver> wait = new FluentWait<WebDriver>(this.driver).withTimeout(timeOutInSeconds, TimeUnit.SECONDS)
                    .pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class);
            wait.until(w -> { return page.isLoaded(); });
        }catch(TimeoutException e){condition=false;}

        return condition;
    }

    public WebElement findElem(WebElement elem, By locator) {
        WebElement foundElem = null;
        try{
            foundElem = elem.findElement(locator);
        } catch (NotFoundException ex) {}
        return foundElem;
    }

    public boolean isActive(WebElement element) {
        final String activeAttr = "active";
        for (String val:element.getAttribute("class").split(" ")) {
            if (val.trim().equalsIgnoreCase(activeAttr)) return true;
        }
        return !Objects.isNull(this.findElem(element, By.cssSelector("."+activeAttr)));
    }

    public Object executeJS(String script, Object... args) {
        return ((JavascriptExecutor) (this.driver)).executeScript(script, args);
    }

    public Object executeJS(WebElement  elem, String script, Object... args) {
        return ((JavascriptExecutor) (elem)).executeScript("script", args);
    }

    public boolean isDisplayed(WebElement element) {
        final String displayAttr = "display: none;";
        return  element.isDisplayed();
    }

    public void selectOptionByVisibleText(WebElement elem, String value) {
        if (elem.getTagName().equalsIgnoreCase("select")) {
            new Select(elem).selectByVisibleText(value);
        }
    }

    public void actionClick(WebElement element) {
        new Actions(driver).click(element).perform();
    }

    public void selectOptionByVisibleText(List<WebElement> liElems, String value) {
        for (WebElement li : liElems) {
            this.scrollTo(li);
            String innertext = li.getAttribute("innerText").trim();
            if(innertext.equalsIgnoreCase(value)){
                actionClick(li);
                break;
            }
        }
    }

    public String getMonth(int index) {
        return PageObjectUtils.MONTHS[index+1];

    }

    public void clearAndEnter(WebElement elem, String text) {
        elem.clear();
        elem.sendKeys(text);
    }



}
