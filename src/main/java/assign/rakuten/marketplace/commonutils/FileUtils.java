package assign.rakuten.marketplace.commonutils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class FileUtils {

    private static Logger LOG = LoggerFactory.getLogger(FileUtils.class);

    public static File getResourceAsFile(String fileName) {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(fileName);
        return new File(resource.getFile());
    }

    public static Properties loadProps(String fileName) throws IOException {
        Properties props = new Properties();
        props.load(new FileReader(getResourceAsFile(fileName)));
        return props;
    }

}
