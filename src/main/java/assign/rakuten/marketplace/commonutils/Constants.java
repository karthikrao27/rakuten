package assign.rakuten.marketplace.commonutils;

import org.apache.commons.lang3.SystemUtils;

public class Constants {

    public static String BASE_DIR = System.getProperty("user.dir");
    public static String FILE_SEPARATOR = System.getProperty("file.separator");
    public static String CHROME_DRIVER_EXECUTABLE ;
    public static String BASE_PROPERTIES = "data.properties";
    public static Long DEFAULT_TIMEOUT = 120L;
    public static String BASE_URL;

    static {
        if(SystemUtils.IS_OS_WINDOWS) {
            CHROME_DRIVER_EXECUTABLE = "lin_chromedriver";
        }else if (SystemUtils.IS_OS_LINUX) {
            CHROME_DRIVER_EXECUTABLE = "chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_EXECUTABLE);
        }else if (SystemUtils.IS_OS_MAC) {
            CHROME_DRIVER_EXECUTABLE = "mac_chromedriver";
        }
    }

    public enum RequiredProperties {
        BASE_URL,
    }

    public static class TimeOut {
        public static final int Normal = 120;
    }

    public static class ErrorMessages {
        public static final String FORM_ERROR_WRONGEMAILPASSWORDCOMBO = "Diese E-Mail-Passwort-Kombination ist uns nicht bekannt. Bitte korrigieren Sie Ihre Eingabe.";
        public static final String FORM_ERROR_EMPTYEMAILPASSWORDCOMBO = "Bitte überprüfen Sie Ihre Eingaben.";
        public static final String EMAILFIELD_ERROR_WRONGFORMAT = "Dies ist keine gültige E-Mail Adresse.";
        public static final String EMAILFIELD_ERROR_EMPTYEMAIL = "Die E-Mail Adresse darf nicht leer sein.";
        public static final String PASSWORDFIELD_ERROR_EMPTYPASSWORD = "";
    }

}
