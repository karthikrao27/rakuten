package assign.rakuten.marketplace.dto;

import java.util.Calendar;
import java.util.Date;

public class UserDto {

    private String email;
    private String password;
    private Calendar calendar;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Calendar getDob() {
        return this.calendar;
    }

    public void setDob(Calendar calendar) {
        this.calendar = calendar;
    }

    public String toString(){
        StringBuilder sB = new StringBuilder();
        sB.append("Email : ").append(this.email)
                .append(", Password : ").append(this.password);
        return sB.toString();
    }
}
