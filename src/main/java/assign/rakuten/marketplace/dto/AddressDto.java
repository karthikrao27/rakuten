package assign.rakuten.marketplace.dto;

import assign.rakuten.marketplace.pages.CheckoutPage;

import java.util.Date;

public class AddressDto {


    public enum AddressType {
        FirmAddress,
        NormalAddress,
        DHL;
    }

    private CheckoutPage.Gender gender;
    private boolean billingAddress;
    private AddressType addressType;
    private String lastName;
    private String firstName;
    private String street;
    private String houseNo;
    private String pinCode;
    private String city;
    private String country;
    private String packStationNumber;
    private String packStationCustomerNumber;
    private Date dob;

    public CheckoutPage.Gender getGender() {
        return gender;
    }

    public void setGender(CheckoutPage.Gender gender) {
        this.gender = gender;
    }

    public boolean isBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(boolean billingAddress) {
        this.billingAddress = billingAddress;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPackStationNumber() {
        return packStationNumber;
    }

    public void setPackStationNumber(String packStationNumber) {
        this.packStationNumber = packStationNumber;
    }

    public String getPackStationCustomerNumber() {
        return packStationCustomerNumber;
    }

    public void setPackStationCustomerNumber(String packStationCustomerNumber) {
        this.packStationCustomerNumber = packStationCustomerNumber;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String toString(){
        StringBuilder sB = new StringBuilder();
        sB.append(this.getGender().name()).append(" ").append(this.getFirstName()).append(" ").append(this.getLastName())
                .append(", " ).append("House/Street Info : ").append(this.getHouseNo()).append("/").append(this.getStreet())
                .append(", ").append("City : ").append(this.getCity()).append(", " ).append("Country : ").append(this.getCountry());
        return sB.toString();

    }

}
