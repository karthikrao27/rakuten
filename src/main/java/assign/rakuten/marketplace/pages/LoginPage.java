package assign.rakuten.marketplace.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class LoginPage extends DiscoveryBasePage {

    @FindBy(css="div.messages")
    List<WebElement> messages;

    @FindBy(css="div.messages>div.message-error")
    List<WebElement> errorMessages;

    @FindBy(css="input[name=loginEmail]")
    private WebElement loginEmailInput;

    @FindBy(xpath="//input[@name='loginEmail']/../..")
    private WebElement loginEmailInputContainer;

    @FindBy(css="input[name=loginPassword]")
    private WebElement loginPasswordInput;

    @FindBy(xpath="//input[@name='loginPassword']/../..")
    private WebElement loginPasswordInputContainer;

    @FindBy(css = "button[value=login]")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void load(){
        isLoaded();
    }

    public boolean isLoaded(){
        return this.util.waitUntil(loginEmailInput, new Function<WebElement, Boolean>() {
            @Override
            public Boolean apply(WebElement element) {
                return element.isDisplayed();
            }
        }, 10);
    }

    public void enterEmail(String emailId) {
        this.util.clearAndEnter(this.loginEmailInput, emailId);
    }

    public void enterPassword(String password) {
        this.util.clearAndEnter(this.loginPasswordInput, password);
    }

    public void clickLoginButton() {
        loginButton.click();
    }

    public List<String> getFormErrorMessages() {
        List<String> eMessages = new ArrayList<>();
        if (errorMessages!=null) {
            for(WebElement errorMessage : errorMessages) {
                eMessages.add(errorMessage.getText());
            }
        }
        return eMessages;
    }

    public String getErrorMessageForEmailField(){
        WebElement errorMessage = this.util.findElem(loginEmailInputContainer, By.tagName("p"));
        return errorMessage!=null?errorMessage.getText():null;
    }


}
