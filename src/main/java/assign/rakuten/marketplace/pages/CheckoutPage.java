package assign.rakuten.marketplace.pages;

import assign.rakuten.marketplace.commonutils.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.ByAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class CheckoutPage extends AbstractBasePage {

    Logger LOG = LoggerFactory.getLogger(CheckoutPage.class);

    @FindBy(css="#steps>ul")
    private WebElement activeStep;

    @FindBy(id="trust-box")
    private WebElement trustContainer;

    /**
     * Contains all Step 1 related methods and webelement definitions
     */
    @FindBy(css="#login-method1")
    private WebElement continueAsGuestOption;

    @FindBy(css="#login-method2")
    private WebElement createNewAccountOption;

    @FindBy(css="#login-method3")
    private WebElement loginOption;

    @FindBy(css="#client_email")
    private WebElement loginEmaiIDInput;

    @FindBy(css="#password")
    private WebElement loginPasswordInput;

    @FindBy(css="#new-customer-email")
    private WebElement newUserEmaiIDInput;

    @FindBy(css="#new-customer-password")
    private WebElement newUserPassword;

    @FindBy(css="#new-customer-password-confirmation")
    private WebElement newUserPasswordConfirmation;

    public void enterLoginCredentials(String userName, String password) {
        if(!this.util.isDisplayed(loginOption)) {
            selectLoginOption();
        }
        this.util.clearAndEnter(loginEmaiIDInput, userName);
        this.util.clearAndEnter(loginPasswordInput, password);
    }

    public void enterNewUserCredentials(String email, String password) {
        this.enterNewUserCredentials(email, password, password);
    }

    public void enterNewUserCredentials(String email, String password, String confirmPassword) {
        if(!this.util.isDisplayed(createNewAccountOption)) {
            selectCreateNewAccountOption();
        }
        this.util.clearAndEnter(newUserEmaiIDInput, email);
        this.util.clearAndEnter(newUserPassword, password);
        this.util.clearAndEnter(newUserPasswordConfirmation, confirmPassword);
    }

    public void selectContinueAsGuestOption() {
        continueAsGuestOption.click();
    }

    public void selectLoginOption() {
        LOG.info("Selecting log option");
        loginOption.click();
    }

    public void selectCreateNewAccountOption() {
        createNewAccountOption.click();
    }

    /**
     * Contains all Step 2 related methods and webelement definitions
     */
    public enum Gender {
        Frau,
        Herr
    }

    @FindBy(css="#privacy")
    private WebElement privacy;

    public void acceptPrivacy() {
        this.util.scrollTo(privacy);
        this.privacy.click();
    }

    @FindBy(css=".form-container:not(.existing-address)")
    private WebElement billingAddressContainer;

    @FindBy(css=".form-container.existing-address .new-address")
    private WebElement addNewAddressContainer;

    @FindBy(css=".form-container.existing-address>ul>li:not(.invoice-used):not(.new-address)")
    private List<WebElement> otherAddressContainer;

    @FindBy(css="#delivery-address-new")
    private WebElement newAddressOption;

    @FindBy(css="#delivery-address-new")
    private List<WebElement> otherAddressOptions;

    @FindBy(css="#delivery-address-invoice")
    private WebElement billingAddressOption;

    @FindBy(css="#email")
    private WebElement emailInput;

    @FindBy(css="#invoice-day-container a.jqTransformSelectOpen")
    private WebElement daySelectorExpandButton;

    @FindBy(css="#invoice-day-container ul>li>a")
    private List<WebElement> daySelectors;

    @FindBy(css="#invoice-month-container a.jqTransformSelectOpen")
    private WebElement monthSelectorExpandButton;

    @FindBy(css="#invoice-month-container ul>li")
    private List<WebElement> monthSelectors;

    @FindBy(css="#invoice-year-container a.jqTransformSelectOpen")
    private WebElement yearSelectorExpandButton;

    @FindBy(css="#invoice-year-container ul>li")
    private List<WebElement> yearSelectors;

    @FindBy(css="button[name=add-address]")
    private WebElement addAddressButton;

    By genderDropDownSelector = By.cssSelector("div.gender-container a.jqTransformSelectOpen");
    By genderOptionsSelector = By.cssSelector(".gender-container ul>li>a");
    By firstNameInputSelector = By.cssSelector(".adress-name.address-first-name");
    By lastNameInputSelector = By.cssSelector(".adress-name:not(.address-first-name)");
    ByAll firmAddressOptionSelector = new ByAll(By.xpath("//input[@id='working-address']"), By.xpath("//input[contains(@id,'deliveryWorkingAddress')]"));
    By additionalAddressSelector = By.cssSelector("#additional-address-check");
    By dhlPackstationSelector = By.xpath("//input[contains(@id,'deliveryAddressesPackstation')]");
    By streetSelector = By.cssSelector("input.street");
    By streetNumberSelector = By.cssSelector("input.street-number");
    By zipCodeSelector = By.cssSelector("input.zip-code");
    By citySelector = By.cssSelector("input.city");
    By countryDropDownSelector = By.cssSelector("div.flags a.jqTransformSelectOpen");
    By countryDropDownSelectorXpath = By.xpath("//input[contains(@id,'delivery-country')]");
    By countryOptionsSelector = By.cssSelector("div.flags ul>li>a");

    private WebElement currentContainer;

    public void selectBillingAddressOption() {
        this.currentContainer = this.billingAddressContainer;
        this.util.scrollTo(this.currentContainer);
        this.billingAddressOption.click();
    }

    public void selectAddAddressOption() {
        this.currentContainer = this.addNewAddressContainer;
        this.util.scrollTo(this.currentContainer);
        this.newAddressOption.click();
        this.util.scrollTo(this.addAddressButton);
    }

    public void clickSaveNewAddress() {
        if (this.addAddressButton.isEnabled()) {
            LOG.info("Add - Address - Button clicked");
            this.addAddressButton.click();
        }else{
            LOG.error("Add - Address - Button is not enabled");
        }
    }

    public void selectOtherAddress(int index) {
        this.currentContainer = this.otherAddressContainer.get(index);
        this.otherAddressOptions.get(index).click();
    }

    public List<WebElement> getSavedAddresses() {
        return this.otherAddressContainer;
    }

    public void selectGender(Gender gender) {
        this.currentContainer.findElement(genderDropDownSelector).click();
        List<WebElement> genderOptions = this.currentContainer.findElements(genderOptionsSelector);
        this.util.selectOptionByVisibleText(genderOptions, gender.name());
    }

    public void enterFirstName(String firstName) {
        WebElement elem = this.currentContainer.findElement(firstNameInputSelector);
        this.util.clearAndEnter(elem, firstName);
    }

    public void enterLastName(String lastName) {
        WebElement elem = this.currentContainer.findElement(lastNameInputSelector);
        this.util.clearAndEnter(elem, lastName);
    }

    public void enterStreet(String streetName) {
        WebElement elem = this.currentContainer.findElement(streetSelector);
        this.util.clearAndEnter(elem, streetName);
    }

    public void enterHouseNumber(String houseName) {
        WebElement elem = this.currentContainer.findElement(streetNumberSelector);
        this.util.clearAndEnter(elem, houseName);
    }

    public void enterZipCode(String zipCode) {
        this.currentContainer.findElement(zipCodeSelector).sendKeys(zipCode);
    }

    public void enterCity(String city) {
        this.currentContainer.findElement(citySelector).sendKeys(city);
    }

    public boolean isEmailEditable() {
        return this.emailInput.isEnabled();
    }

    public void enterEmail(String email) {
        this.emailInput.sendKeys(email);
    }

    public void selectCountry(String country) {
        this.currentContainer.findElement(countryDropDownSelector).click();
        List<WebElement> countries = this.currentContainer.findElements(countryOptionsSelector);
        this.util.sleep(2);
        this.util.selectOptionByVisibleText(countries, country);
    }

    public void selectDobDay(String day) {
        this.daySelectorExpandButton.click();
        this.util.selectOptionByVisibleText(this.daySelectors, day);
    }

    public void selectDobMonth(String month) {
        this.monthSelectorExpandButton.click();
        this.util.selectOptionByVisibleText(this.monthSelectors, month);
    }

    public void selectDobYear(String year) {
        this.yearSelectorExpandButton.click();
        this.util.selectOptionByVisibleText(this.yearSelectors, year);
    }

    /**
     * Contains info common to all Steps
     */
    @FindBy(css="#go_to_next_step")
    private WebElement goToNextStepButton;

    public void goToNextStep(final Step step) {
        this.goToNextStepButton.click();
        this.util.waitUntil(new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return step.equals(getCurrentStep());
            }
        }, Constants.TimeOut.Normal);
        this.load();
    }

    //Gets the current Step
    private Step currentStep;

    public enum Step {
        STEP_1("step-1"),
        STEP_2("step-2"),
        STEP_3("step-3"),
        STEP_4("step-4");

        private String stepVal;

        private Step(String stepVal) {
            this.stepVal = stepVal;
        }

        public static Step parseByVal(String val) {
            for(Step step : Step.values()) {
                if (step.stepVal.equalsIgnoreCase(val)) return step;
            }
            return null;
        }
    }

    public Step getCurrentStep() {
        return Step.parseByVal(activeStep.getAttribute("class").replace("current-",""));
    }

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public void load() {
        PageFactory.initElements(this.driver, this);
        isLoaded();
        LOG.info("Getting Current Step");
        currentStep = this.getCurrentStep();

        LOG.info("Current Step is {}", currentStep.name());
        boolean loaded = false;
        switch (currentStep) {
            case STEP_1:
                loaded = isStepOneLoaded();
                break;
            case STEP_2:
                loaded = isStepTwoLoaded();
                break;
            case STEP_3:
                loaded = isStepThreeLoaded();
                break;
            case STEP_4:
                loaded = isStepFourLoaded();
                break;
        }
        if (!loaded) LOG.info("Full Page seems like not loaded");
    }

    public boolean isLoaded() {
        return this.util.waitUntil(goToNextStepButton, new Function<WebElement, Boolean>() {
            @Override
            public Boolean apply(WebElement element) {
                return element.isDisplayed();
            }
        }, Constants.TimeOut.Normal);
    }

    public boolean isStepOneLoaded() {
        return this.util.waitUntil(createNewAccountOption, new Function<WebElement, Boolean>() {
            @Override
            public Boolean apply(WebElement element) {
                return element.isDisplayed();
            }
        }, Constants.TimeOut.Normal);
    }

    public boolean isStepTwoLoaded() {
        return this.util.waitUntil(privacy, new Function<WebElement, Boolean>() {
            @Override
            public Boolean apply(WebElement element) {
                return element.isDisplayed();
            }
        }, Constants.TimeOut.Normal);
    }

    public boolean isStepThreeLoaded() {
        return this.util.waitUntil(new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return driver.getCurrentUrl().endsWith("checkout/payment");
            }
        }, Constants.TimeOut.Normal);
    }

    public boolean isStepFourLoaded() {
        return this.util.waitUntil(createNewAccountOption, new Function<WebElement, Boolean>() {
            @Override
            public Boolean apply(WebElement element) {
                return element.isDisplayed();
            }
        }, Constants.TimeOut.Normal);
    }

}
