package assign.rakuten.marketplace.pages;

import assign.rakuten.marketplace.commonutils.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.function.Function;

public class CartPage extends AbstractBasePage{

    @FindBy(css="#summarized-total .amount")
    private WebElement summarizedTotal;

    @FindBy(css="#go_to_checkout")
    private WebElement goToCheckoutButton;

    public CartPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void load() {
        PageFactory.initElements(this.driver, this);
        isLoaded();
    }

    @Override
    public boolean isLoaded() {
        return this.util.waitUntil(goToCheckoutButton, new Function<WebElement, Boolean>() {
            @Override
            public Boolean apply(WebElement element) {
                return element.isDisplayed();
            }
        }, Constants.TimeOut.Normal);
    }

    public void checkOut() {
        goToCheckoutButton.click();
    }

}
