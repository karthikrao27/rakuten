package assign.rakuten.marketplace.pages;

import assign.rakuten.marketplace.commonutils.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.function.Function;

class Header extends AbstractBasePage {

    @FindBy(css="a.b-brand-logo")
    private WebElement logo;

    @FindBy(css="#search_form #q")
    private WebElement searchBar;

    public Header(final WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    @Override
    public void load() {
        PageFactory.initElements(this.driver, this);
        isLoaded();
    }

    public boolean isLoaded() {
        return this.util.waitUntil((Function<WebDriver, Boolean>) p -> {return this.logo.isDisplayed();}, Constants.TimeOut.Normal);
    }

    @Override
    public void clickOnLogo() {
        this.logo.click();
    }

}
