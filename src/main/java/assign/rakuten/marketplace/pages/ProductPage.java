package assign.rakuten.marketplace.pages;

import assign.rakuten.marketplace.commonutils.Constants;
import com.google.common.base.Function;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class ProductPage extends DiscoveryBasePage {

    private static Logger LOG = LoggerFactory.getLogger(ProductPage.class);

    private String url;

    @FindBy(css="form[name=cart] #cardButton")
    private WebElement addToCartButton;

    @FindBy(css="form[name=cart] input[name=amount]")
    private WebElement quantityInput;

    @FindBy(css="#atc_b_tsc")
    private WebElement goToCartButton;

    @FindBy(css="#atc_b_cs")
    private WebElement continueShoppingButton;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public ProductPage(WebDriver driver, String url) {
        this(driver);
        this.url = url;
        this.load();
    }

    public void load(){
        PageFactory.initElements(driver, this);
        if (!Objects.isNull(url) && this.driver.getCurrentUrl() != this.url){
            LOG.info("Loading PDP Url {}", url);
            this.util.loadPage(url);
        }
        isLoaded();
    }

    public boolean isLoaded() {
        return super.isLoaded() && this.util.waitUntil(addToCartButton, (Function<WebElement, Boolean>) elem -> { return elem.isDisplayed(); }, Constants.TimeOut.Normal);
    }

    public CartPage addToCart(boolean goToCart) {
        addToCartButton.click();
        this.util.waitUntil(goToCartButton, (Function<WebElement, Boolean>) elem -> { return elem.isDisplayed(); }, Constants.TimeOut.Normal);
        if (goToCart) {
            goToCartButton.click();
        } else {
            continueShoppingButton.click();
        }
        LOG.info("Added to Cart");
        CartPage cartPage = new CartPage(driver);
        LOG.info("Proceeding to Cart");
        cartPage.load();
        return cartPage;
    }

}
