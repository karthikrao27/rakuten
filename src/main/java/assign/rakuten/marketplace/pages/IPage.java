package assign.rakuten.marketplace.pages;

public interface IPage {

    public void load();
    public boolean isLoaded();
}
