package assign.rakuten.marketplace.pages;

import org.openqa.selenium.WebDriver;

public abstract class DiscoveryBasePage extends Header {

    public DiscoveryBasePage(WebDriver driver) {
        super(driver);
    }
}
