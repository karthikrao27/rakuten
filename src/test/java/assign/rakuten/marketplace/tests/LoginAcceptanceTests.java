package assign.rakuten.marketplace.tests;

import assign.rakuten.marketplace.commonutils.Constants;
import assign.rakuten.marketplace.dto.UserDto;
import assign.rakuten.marketplace.tests.annotations.SkipHomePageLoad;
import org.testng.annotations.Test;

@SkipHomePageLoad
public class LoginAcceptanceTests extends BaseTest{


    @Test(description = "To verify successful login happens on providing correct credentials")
    public void verifySuccessfulLogin() {
        getLoginHelper().loadLoginPage(getBaseUrl());
        UserDto loggedInUser = getTestDataProvider().getDefaultLoggedInUserDto();
        getLoginHelper().login(loggedInUser.getEmail(), loggedInUser.getPassword());
        getLoginHelper().assertLoginSuccessful();
    }

    @Test(description = "To verify error message on providing wrong password")
    public void verifyLoginForWrongPassword() {
        getLoginHelper().loadLoginPage(getBaseUrl());
        UserDto loggedInUser = getTestDataProvider().getDefaultLoggedInUserDto();
        getLoginHelper().login(loggedInUser.getEmail(), "wrong password");
        getLoginHelper().assertErrorMessageAppeared(Constants.ErrorMessages.FORM_ERROR_WRONGEMAILPASSWORDCOMBO);
    }

    @Test(description = "To verify error message on providing wrong email")
    public void verifyLoginForWrongEmail() {
        getLoginHelper().loadLoginPage(getBaseUrl());
        UserDto loggedInUser = getTestDataProvider().getDefaultLoggedInUserDto();
        getLoginHelper().login("wrongemail@wrongemail.com", "wrong password");
        getLoginHelper().assertErrorMessageAppeared(Constants.ErrorMessages.FORM_ERROR_WRONGEMAILPASSWORDCOMBO);
    }

    @Test(description = "To verify error message on providing wrong email format")
    public void verifyLoginForWrongEmailFormat() {
        getLoginHelper().loadLoginPage(getBaseUrl());
        UserDto loggedInUser = getTestDataProvider().getDefaultLoggedInUserDto();
        getLoginHelper().login("wrongemailformat@@@@", "wrong password");
        getLoginHelper().assertErrorMessageAppeared(Constants.ErrorMessages.FORM_ERROR_EMPTYEMAILPASSWORDCOMBO);
        getLoginHelper().assertErrorMessageAppearedForEmailField(Constants.ErrorMessages.EMAILFIELD_ERROR_WRONGFORMAT);

    }

    @Test(description = "To verify error message on submitting empty email and password")
    public void verifyLoginForEmptyEmailEmptyPassword() {
        getLoginHelper().loadLoginPage(getBaseUrl());
        UserDto loggedInUser = getTestDataProvider().getDefaultLoggedInUserDto();
        getLoginHelper().login("", "");
        getLoginHelper().assertErrorMessageAppeared(Constants.ErrorMessages.FORM_ERROR_EMPTYEMAILPASSWORDCOMBO);
        getLoginHelper().assertErrorMessageAppearedForEmailField(Constants.ErrorMessages.EMAILFIELD_ERROR_EMPTYEMAIL);
    }


}
