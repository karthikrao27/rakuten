package assign.rakuten.marketplace.tests;

import assign.rakuten.marketplace.dto.AddressDto;
import assign.rakuten.marketplace.dto.UserDto;
import assign.rakuten.marketplace.pages.CheckoutPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class WorkflowTests extends BaseTest {
    Logger LOG = LoggerFactory.getLogger(WorkflowTests.class);

    @Test(description = "To Verify Existing logged-in User is able to checkout to the Payment page with Default Billing Address")
    public void verifyCheckoutAsExistingLoginUser() {
        UserDto loggedInUser = getTestDataProvider().getDefaultLoggedInUserDto();
        LOG.info("Logged-in User info - {}", loggedInUser.toString());
        getCheckoutHelper().checkoutAsLoggedInUser(getPdpUrl(getProperty("normalPdpUrl")), loggedInUser);
        getCheckoutHelper().assertCheckOutInStep(CheckoutPage.Step.STEP_3);
    }

    @Test(description = "To Verify Existing logged-in User is able to checkout to the Payment page with New Address")
    public void verifyCheckoutAsExistingLoginUserWithNewAddress() {
        UserDto loggedInUser = getTestDataProvider().getDefaultLoggedInUserDto();
        LOG.info("Logged-in User info - {}", loggedInUser.toString());
        AddressDto newAddress = getTestDataProvider().getNewAddress(AddressDto.AddressType.NormalAddress);
        LOG.info("Address info - {}", newAddress.toString());
        getCheckoutHelper().checkoutAsLoggedInUser(getPdpUrl(getProperty("normalPdpUrl")), loggedInUser, newAddress);
        getCheckoutHelper().assertCheckOutInStep(CheckoutPage.Step.STEP_3);
    }

    @Test(description = "To Verify Guest User is able to checkout to the Payment page")
    public void verifyCheckoutAsGuest() {
        UserDto guestUser = getTestDataProvider().getDefaultGuestUserDto();
        LOG.info("Guest User info - {}", guestUser.toString());
        AddressDto newAddress = getTestDataProvider().getNewAddress(AddressDto.AddressType.NormalAddress);
        LOG.info("Address info - {}", newAddress.toString());
        getCheckoutHelper().checkoutAsGuest(getPdpUrl(getProperty("normalPdpUrl")), guestUser, newAddress);
        getCheckoutHelper().assertCheckOutInStep(CheckoutPage.Step.STEP_3);
    }


}
