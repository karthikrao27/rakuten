package assign.rakuten.marketplace.tests.helper;

import assign.rakuten.marketplace.dto.AddressDto;
import assign.rakuten.marketplace.dto.UserDto;
import assign.rakuten.marketplace.pages.CheckoutPage;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.util.Calendar;
import java.util.function.Function;

public class CheckoutHelper extends BaseHelper {

    Logger LOG = LoggerFactory.getLogger(CheckoutHelper.class);

    private CheckoutPage checkoutPage;
    private PdpHelper pdpHelper;
    private CartHelper cartHelper;

    public CheckoutHelper(WebDriver driver) {
        super(driver);
        this.checkoutPage = new CheckoutPage(driver);
        this.cartHelper = new CartHelper(driver);
        this.pdpHelper = new PdpHelper(driver);
    }

    public void load() {
        this.checkoutPage.load();
    }

    public void checkoutAsGuest(String pdpUrl, UserDto userDto, AddressDto addressDto) {
        LOG.info("Checking out as Guest");
        this.cartHelper.proceedToCheckout(pdpUrl);
        this.checkoutPage.load();
        LOG.info("Selecting Guest Option");
        this.checkoutPage.selectContinueAsGuestOption();
        LOG.info("Proceeding to Step 2");
        this.checkoutPage.goToNextStep(CheckoutPage.Step.STEP_2);
        LOG.info("Adding new Address");
        this.addNewBillingAddress(addressDto, userDto);
        LOG.info("Accepting Privacy");
        this.checkoutPage.acceptPrivacy();
        LOG.info("Proceeding to next step");
        this.checkoutPage.goToNextStep(CheckoutPage.Step.STEP_3);
    }

    public void addNewBillingAddress(AddressDto addressDto, UserDto userDto) {
        this.checkoutPage.selectBillingAddressOption();
        this.checkoutPage.selectGender(addressDto.getGender());
        this.checkoutPage.enterFirstName(addressDto.getFirstName());
        this.checkoutPage.enterLastName(addressDto.getLastName());
        this.checkoutPage.enterHouseNumber(addressDto.getHouseNo());
        this.checkoutPage.enterStreet(addressDto.getStreet());
        this.checkoutPage.enterZipCode(addressDto.getPinCode());
        this.checkoutPage.enterCity(addressDto.getCity());
        this.checkoutPage.selectCountry(addressDto.getCountry());
        if (this.checkoutPage.isEmailEditable()) {
            this.checkoutPage.enterEmail(userDto.getEmail());
            Calendar cal = userDto.getDob();
            this.checkoutPage.selectDobDay(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
            this.checkoutPage.selectDobMonth(this.getMonth(cal.get(Calendar.MONTH)));
            this.checkoutPage.selectDobYear(String.valueOf(cal.get(Calendar.YEAR)));
        }
    }

    public void addNewAddress(AddressDto addressDto) {
        final int savedAddresses = this.checkoutPage.getSavedAddresses().size();
        this.checkoutPage.selectAddAddressOption();
        this.checkoutPage.selectGender(addressDto.getGender());
        this.checkoutPage.enterFirstName(addressDto.getFirstName());
        this.checkoutPage.enterLastName(addressDto.getLastName());
        this.checkoutPage.enterHouseNumber(addressDto.getHouseNo());
        this.checkoutPage.enterStreet(addressDto.getStreet());
        this.checkoutPage.enterZipCode(addressDto.getPinCode());
        this.checkoutPage.enterCity(addressDto.getCity());
        this.checkoutPage.selectCountry(addressDto.getCountry());
        this.checkoutPage.clickSaveNewAddress();
        this.sleepTill(this.checkoutPage, new Function<CheckoutPage, Boolean>() {
            @Override
            public Boolean apply(CheckoutPage checkoutPage) {
                return (checkoutPage.getSavedAddresses().size()+1)==savedAddresses;
            }
        },10);
    }

    public void checkoutAsLoggedInUser(String pdpUrl, UserDto userDto) {
        LOG.info("Proceeding to checkout {}", pdpUrl);
        this.cartHelper.proceedToCheckout(pdpUrl);
        this.checkoutPage.load();
        LOG.info("Selecting Login Option");
        this.checkoutPage.selectLoginOption();
        LOG.info("Entering Login Credentials {}, {}", userDto.getEmail(), userDto.getPassword());
        this.checkoutPage.enterLoginCredentials(userDto.getEmail(), userDto.getPassword());
        LOG.info("Proceeding to Step 2");
        this.checkoutPage.goToNextStep(CheckoutPage.Step.STEP_2);
        LOG.info("Selecting Billing Address option");
        this.checkoutPage.selectBillingAddressOption();
        LOG.info("Accepting Privacy");
        this.checkoutPage.acceptPrivacy();
        LOG.info("Proceeding to next step");
        this.checkoutPage.goToNextStep(CheckoutPage.Step.STEP_3);
    }

    public void checkoutAsLoggedInUser(String pdpUrl, UserDto userDto, AddressDto addressDto) {
        LOG.info("Proceeding to checkout {}", pdpUrl);
        this.cartHelper.proceedToCheckout(pdpUrl);
        this.checkoutPage.load();
        LOG.info("Selecting Login Option");
        this.checkoutPage.selectLoginOption();
        LOG.info("Entering Login Credentials {}, {}", userDto.getEmail(), userDto.getPassword());
        this.checkoutPage.enterLoginCredentials(userDto.getEmail(), userDto.getPassword());
        LOG.info("Proceeding to Step 2");
        this.checkoutPage.goToNextStep(CheckoutPage.Step.STEP_2);
        LOG.info("Adding new Address");
        this.addNewAddress(addressDto);
        LOG.info("Accepting Privacy");
        this.checkoutPage.acceptPrivacy();
        LOG.info("Proceeding to next step");
        this.checkoutPage.goToNextStep(CheckoutPage.Step.STEP_3);
    }

    public void assertCheckOutInStep(CheckoutPage.Step expectedStep) {
        LOG.info("Asserting Checkout page is in {}", expectedStep.name());
        Assert.assertEquals(this.checkoutPage.getCurrentStep(), expectedStep);
        LOG.info("Asserted Successfully");
    }

}
