package assign.rakuten.marketplace.tests.helper;

import assign.rakuten.marketplace.pages.CartPage;
import assign.rakuten.marketplace.pages.ProductPage;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CartHelper {

    private static Logger LOG = LoggerFactory.getLogger(CartHelper.class);

    private WebDriver driver;
    private CartPage cartPage;
    private PdpHelper pdpHelper;

    public CartHelper (WebDriver driver) {
        this.driver = driver;
        this.cartPage = new CartPage(driver);
        this.pdpHelper = new PdpHelper(driver);
    }

    public void load() {
        this.cartPage.load();
    }

    public void proceedToCheckout(String pdpUrl) {
        this.pdpHelper.addToCart(pdpUrl, true);
        this.cartPage.load();
        this.cartPage.checkOut();
    }

    public void proceedToCheckout(ProductPage pdpPage) {
        pdpPage.addToCart(true);
        this.cartPage.load();
        LOG.info("Cart page loaded");
        this.cartPage.checkOut();
        LOG.info("Checking out cart");
    }



}
