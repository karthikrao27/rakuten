package assign.rakuten.marketplace.tests.helper;

import assign.rakuten.marketplace.pages.ProductPage;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PdpHelper {

    private static Logger LOG = LoggerFactory.getLogger(PdpHelper.class);

    private WebDriver driver;
    private ProductPage pdpPage;

    public PdpHelper(WebDriver driver) {
        this.driver = driver;
    }

    public void load(String pdpUrl) {
        this.pdpPage = new ProductPage(this.driver, pdpUrl);
    }

    public void addToCart(String pdpUrl, boolean goToCart) {
        if (this.driver.getCurrentUrl()!=pdpUrl) this.load(pdpUrl);
        this.pdpPage.addToCart(goToCart);
    }


}
