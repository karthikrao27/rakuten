package assign.rakuten.marketplace.tests.helper;

import assign.rakuten.marketplace.commonutils.Constants;
import assign.rakuten.marketplace.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class LoginHelper extends BaseHelper {

    private static Logger LOG = LoggerFactory.getLogger(LoginHelper.class);

    public static final String URL = "/kundenkonto";

    private LoginPage loginPage;

    public LoginHelper(WebDriver driver) {
        super(driver);
        loginPage = new LoginPage(driver);
    }

    public void loadLoginPage(String baseUrl) {
        String loginUrl = baseUrl + URL;
        this.loadPage(loginUrl);
        loginPage.load();
        LOG.info("Login page is loaded");
    }

    public void login(String emailId, String pwd) {
        LOG.info("Entering email");
        this.loginPage.enterEmail(emailId);
        LOG.info("Entering password");
        this.loginPage.enterPassword(pwd);
        LOG.info("Submitting credentials");
        this.loginPage.clickLoginButton();
    }

    public void assertLoginSuccessful(){
        LOG.info("Checking Login is successful");
        boolean login = this.waitUntil(new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return driver.getCurrentUrl().contains("/kundenkonto/uebersicht");
            }
        }, Constants.TimeOut.Normal);
        Assert.assertTrue(login, "Login is not successful");
        LOG.info("Login is successful");
    }

    public void assertErrorMessageAppeared(String errorMessage){
        List<String> errorMessages = this.loginPage.getFormErrorMessages();
        for(String message : errorMessages) {
            if (message.equals(errorMessage)) return;
        }
        Assert.fail("Error message : '" + errorMessage + "' hasn't appeared. Error Messages listed : " + Arrays.toString(errorMessages.toArray()));
    }

    public void assertErrorMessageAppearedForEmailField(String expectedErrorMessage){
        Assert.assertEquals(this.loginPage.getErrorMessageForEmailField(), expectedErrorMessage);
    }

}
