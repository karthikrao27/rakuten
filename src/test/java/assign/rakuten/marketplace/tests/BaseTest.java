package assign.rakuten.marketplace.tests;

import assign.rakuten.marketplace.commonutils.Constants;
import assign.rakuten.marketplace.commonutils.Constants.RequiredProperties;
import assign.rakuten.marketplace.commonutils.FileUtils;
import assign.rakuten.marketplace.tests.annotations.SkipHomePageLoad;
import assign.rakuten.marketplace.tests.data.TestDataProvider;
import assign.rakuten.marketplace.tests.helper.CartHelper;
import assign.rakuten.marketplace.tests.helper.CheckoutHelper;
import assign.rakuten.marketplace.tests.helper.LoginHelper;
import assign.rakuten.marketplace.tests.helper.PdpHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    private static Logger LOG = LoggerFactory.getLogger(BaseTest.class);

    private WebDriver driver;
    private ChromeDriverService driverService;
    private CheckoutHelper checkoutHelper;
    private CartHelper cartHelper;
    private PdpHelper pdpHelper;
    private LoginHelper loginHelper;
    private TestDataProvider testDataProvider;
    private Properties baseProps;

    private String baseUrl;

    @BeforeSuite(alwaysRun = true)
    public void beforeSuiteBase() throws IOException {
        LOG.info("Before Suite - Base : started");
        this.driverService = new ChromeDriverService.Builder()
                .usingDriverExecutable(FileUtils.getResourceAsFile(Constants.CHROME_DRIVER_EXECUTABLE))
                .usingAnyFreePort()
                .build();
        this.driverService.start();
        this.testDataProvider = new TestDataProvider();
        this.baseProps = FileUtils.loadProps(Constants.BASE_PROPERTIES);
        this.loadConstants(baseProps);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethodBase(Method testMethod) throws IOException {
        LOG.info("{} method started", testMethod.getName().toUpperCase());
        LOG.info("----------------------------------------------------");

        LOG.info("Initializing Driver");
        setupDriver();

        LOG.info("Initializing Helpers");
        initHelpers();

        LOG.info("Loading PreConditions");
        loadConditions(testMethod);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethodBase(Method testMethod, ITestContext context) {
        if (this.driver != null) this.driver.quit();
        LOG.info("----------------------------------------------------");
        LOG.info("{} - method ended", testMethod.getName().toUpperCase());
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuiteBase() {
        if (this.driverService != null) this.driverService.stop();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public CheckoutHelper getCheckoutHelper() {
        return checkoutHelper;
    }

    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    public TestDataProvider getTestDataProvider() {
        return testDataProvider;
    }

    public String getProperty(String key) {
        return this.baseProps.getProperty(key);
    }

    private void loadConstants(Properties props) {
        for (RequiredProperties requiredProperty : RequiredProperties.values()) {
            String requiredVal = props.getProperty(requiredProperty.name());
            if (requiredVal!=null) {
                try {
                    Constants.class.getField(requiredProperty.name()).set(new Constants(),requiredVal);
                } catch (NoSuchFieldException e) { } catch (IllegalAccessException e) { }
            }
        }
    }


    private void setupDriver() throws IOException {
        if(this.driverService==null || !this.driverService.isRunning()) beforeSuiteBase();
        this.driver = new ChromeDriver(this.driverService);
        this.driver.manage().timeouts().implicitlyWait(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        this.driver.manage().timeouts().setScriptTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
    }

    private void initHelpers() {
        this.loginHelper = new LoginHelper(this.driver);
        this.pdpHelper = new PdpHelper(this.driver);
        this.cartHelper = new CartHelper(this.driver);
        this.checkoutHelper = new CheckoutHelper(this.driver);
    }

    public void loadConditions(Method m) {
         if(!isAnnotatedWith(m, SkipHomePageLoad.class)){
             LOG.info("Loading base url {}", getBaseUrl());
             this.driver.get(getBaseUrl());
         }
    }

    private <T> boolean isAnnotatedWith(Method m, Class<T> annotationClass) {
        Annotation[] annotations = m.getDeclaredAnnotations();
        for(Annotation annotation : annotations) {
            if(annotation.annotationType().getName().equals(annotationClass.getName())){
                return true;
            }
        }
        annotations = m.getDeclaringClass().getDeclaredAnnotations();
        for(Annotation annotation : annotations) {
            if(annotation.annotationType().getName().equals(annotationClass.getName())){
                return true;
            }
        }
        return false;
    }

    public String getPdpUrl(String pdpUrl) {
        return getBaseUrl() + pdpUrl;
    }

    public String getBaseUrl() {
        return Constants.BASE_URL;
    }

}
