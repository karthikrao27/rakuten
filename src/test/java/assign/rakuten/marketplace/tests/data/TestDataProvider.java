package assign.rakuten.marketplace.tests.data;

import assign.rakuten.marketplace.dto.AddressDto;
import assign.rakuten.marketplace.dto.UserDto;
import assign.rakuten.marketplace.pages.CheckoutPage;
import org.testng.annotations.DataProvider;

import java.security.SecureRandom;
import java.util.*;

public class TestDataProvider {

    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String ALPHANUMERIC = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public String getRandomString(int length) {
        StringBuilder sB = new StringBuilder();
        for(int i=0;i<length;i++){
            sB.append(ALPHA.charAt(new SecureRandom().nextInt(ALPHA.length())));
        }
        return sB.toString();
    }

    public String getRandomNumber(int length) {
        StringBuilder sB = new StringBuilder();
        for(int i=0;i<length;i++){
            sB.append(new SecureRandom().nextInt(10));
        }
        return sB.toString();
    }

    public String getRandomNumber(int length, int... excludedNos) {
        List<Integer> excluded = new ArrayList<>();
        for (int excludedNumber : excludedNos) {
            excluded.add(excludedNumber);
        }
        StringBuilder sB = new StringBuilder();
        while(sB.length()<length){
            int no = new SecureRandom().nextInt(10);
            if (excluded.contains(no)) {
                continue;
            }
            sB.append(no);
        }
        return sB.toString();
    }


    public UserDto getDefaultGuestUserDto() {
        UserDto guestUser = new UserDto();
        guestUser.setEmail("rakutenusertest@gmail.com");
        Calendar cal = Calendar.getInstance();
        cal.set(2000, 10, 20);
        guestUser.setDob(cal);
        return  guestUser;
    }

    public UserDto getDefaultLoggedInUserDto() {
        UserDto loggedInUser = new UserDto();
        loggedInUser.setEmail("karthik.rao.27@gmail.com");
        loggedInUser.setPassword("Test@123");
        return  loggedInUser;
    }

    public AddressDto getDefaultBillingAddress() {
        AddressDto defaultAddress = new AddressDto();
        return defaultAddress;
    }

    public AddressDto getNewAddress(AddressDto.AddressType addressType) {
        switch (addressType){
            case NormalAddress:
                return getNewNormalAddress();
            default:
                return null;
        }
    }

    private AddressDto getNewNormalAddress() {
        AddressDto newAddress = new AddressDto();
        newAddress.setAddressType(AddressDto.AddressType.NormalAddress);
        newAddress.setGender(CheckoutPage.Gender.Herr);
        newAddress.setLastName(getRandomString(4));
        newAddress.setFirstName("First");
        newAddress.setHouseNo(getRandomNumber(4,0));
        newAddress.setStreet("Europa-Allee");
        newAddress.setCity("Frankfurt");
        newAddress.setPinCode("60327");
        newAddress.setCountry("Deutschland");
        return newAddress;
    }


}
